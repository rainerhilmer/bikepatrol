# Police partner on bike #
This mod enables your police partner to ride his own police bike to go on bike patrol with you.
It's an "install and forget" mod.
That means, once installed you don't need to care for anything.
Just hop on your bike and your partner will do the same with his bike and follow you everywhere you go.
You don't even need to care for his bike. In case he has none the mod will spawn a bike for him.
BTW: This will not only work in case you are on a police bike but always in case you get into a vehicle which has no passenger seat free.

### Current project status ###
RELEASED

### Who you can talk to ###

* Rainer Hilmer (mailto: cyron43@web.de)