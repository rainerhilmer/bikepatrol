﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cyron43.GtaIV.Common;
using GTA;

namespace BikePatrol
{
   public class Core : Script
   {
      private const float NEAR = 5.0f;
      private static readonly object SyncLock = new object();
      private int _actualInterval;
      private Ped _partner;
      private Vehicle _partnerBike;
      private Vehicle _playerVehicle;

      public Core()
      {
         Interval = CommonFunctions.LONG_INTERVAL;
         Tick += OnIdleState;
      }

      private static Ped RemoveBodyguardAttributes(ref Ped partner)
      {
         partner.BlockPermanentEvents = true;
         partner.ChangeRelationship(RelationshipGroup.Cop, Relationship.Neutral);
         //question: more?
         return partner;
      }

      private bool ConditionsForFollowingAreGiven()
      {
         if(Player.Character.RelationshipGroup != RelationshipGroup.Cop)
            return false;
         if(!CommonFunctions.PedExists(_partner))
            _partner = GetClosestCop();
         if(!CommonFunctions.PedExists(_partner))
            return false;
         if(!_partner.isAliveAndWell)
            return false;
         if(CommonFunctions.PedExists(Player.Character) && !Player.Character.isSittingInVehicle())
            return false;
         if(PartnerCanRideInPlayersVehicle())
            return false;
         DisposePartnerBikeIfTooFarAwayFromPartner();
         if(!CommonFunctions.VehicleExists(_partnerBike))
            GetPoliceBikeForPartner();
         if(CommonFunctions.PedExists(Player.Character)
            && CommonFunctions.VehicleExists(_partnerBike)
            && Player.Character.isSittingInVehicle(_partnerBike))
            GetPoliceBikeForPartner();
         return PartnerAndPartnerVehicleExist();
      }

      private void DisposePartnerBikeIfTooFarAwayFromPartner()
      {
         if(!CommonFunctions.VehicleExists(_partnerBike))
            return;
         if(_partnerBike.Position.DistanceTo(_partner.Position) > 20.0f)
            _partnerBike.Delete();
      }

      /// <summary>
      /// The closest cop is always you! So get the nearest one with a distance > 0.
      /// </summary>
      /// <returns>That ought to be your partner.</returns>
      private Ped GetClosestCop()
      {
         var pedsAround = CommonFunctions.GetPedsSafe(Player.Character.Position, NEAR);
         if(pedsAround == null)
            return null;
         if(pedsAround.Length == 0)
            return null;
         var copsAround = pedsAround.Where(ped => ped.RelationshipGroup == RelationshipGroup.Cop).ToList();
         if(copsAround.Count == 0)
            return null;
         Ped partner = null;
         foreach(var cop in copsAround)
         {
            if(partner == null
               && cop.Position.DistanceTo2D(Player.Character.Position) > 0)
               partner = cop;
            if(partner != null
               && cop.Position.DistanceTo2D(Player.Character.Position)
               < partner.Position.DistanceTo2D(Player.Character.Position))
               partner = cop;
         }
         if(partner == null)
            return null;
         partner = RemoveBodyguardAttributes(ref partner);
         return partner;
      }

      private Vehicle GetClosestPoliceBike(IEnumerable <Vehicle> policeBikesAround)
      {
         Vehicle closestBike = null;
         foreach(var bike in policeBikesAround)
         {
            if(closestBike == null
               && bike.Position.DistanceTo2D(Player.Character.Position) > 0
               && bike.isSeatFree(VehicleSeat.Driver)
               && bike.isDriveable)
               closestBike = bike;
            else if(closestBike != null
                    && bike.isSeatFree(VehicleSeat.Driver)
                    && bike.Position.DistanceTo2D(Player.Character.Position)
                    < closestBike.Position.DistanceTo2D(Player.Character.Position)
                    && bike.isDriveable)
               closestBike = bike;
         }
         return closestBike;
      }

      private void GetPoliceBikeForPartner()
      {
         byte min = 1;
         if(_playerVehicle.Model == new Model("POLICEB"))
            min = 2; // the player's bike counts towards bikes around.
         var policeBikesAround = CommonFunctions.GetVehiclesSafe(Player.Character.Position, 10.0f, new Model("POLICEB"));
         if(policeBikesAround.Length < min)
            _partnerBike = SpawnPoliceBike();
         if(policeBikesAround.Length > 1)
            _partnerBike = GetClosestPoliceBike(policeBikesAround);
         if(!CommonFunctions.VehicleExists(_partnerBike))
            _partnerBike = SpawnPoliceBike();
         else
            _partnerBike.AllowSirenWithoutDriver = true;
      }

      private void OnIdleState(object sender, EventArgs e)
      {
         if(PartnerIsCloseAndHeIsOnHisBikeAndPlayerIsOnFoot())
            PartnerLeaveBike();
         if(!ConditionsForFollowingAreGiven())
            return;
         SynchronizePartnerSiren();
         if(!PartnerIsOnHisBike())
         {
            _partner.Task.AlwaysKeepTask = true;
            PartnerGetOnYourBike();
         }
         if(_partner.Position.DistanceTo(Player.Character.Position) > NEAR)
            SwitchTickEventToPartnerFollowPlayer();
      }

      private void OnPartnerFollowPlayer(object sender, EventArgs e)
      {
         if(!PartnerAndPartnerVehicleExist())
         {
            SwitchTickEventToIdleState();
            return;
         }
         if(PartnerIsCloseAndHeIsOnHisBikeAndPlayerIsOnFoot())
         {
            PartnerLeaveBike();
            SwitchTickEventToIdleState();
            return;
         }
         if(_partner == null)
            return;
         PartnerHopOnBikeIfNotAlreadyInCaseYouAreFarAway();
         _partner.Task.AlwaysKeepTask = true;
         if(PartnerIsInWrongVehicle())
         {
            _partner.LeaveVehicle();
            PartnerGetOnYourBike();
         }
         var speed = DrivingBehavior.AdaptSpeed(
            _playerVehicle.Speed, Player.Character.Position, _partner.Position);
         _partner.Task.DriveTo(
            DrivingBehavior.PlayerOffsetPosition(Player.Character, 1.0f),
            CommonFunctions.CorrectedSpeed(speed), false, true);
         TeleportIfTooFarAway();
         SynchronizePartnerSiren();
      }

      private bool PartnerAndPartnerVehicleExist()
      {
         return CommonFunctions.PedExists(_partner) && CommonFunctions.VehicleExists(_partnerBike);
      }

      private bool PartnerCanRideInPlayersVehicle()
      {
         if(Player.Character.CurrentVehicle == null)
            return false;
         _playerVehicle = Player.Character.CurrentVehicle;
         if(Player.Character.CurrentVehicle.isSeatFree(VehicleSeat.AnyPassengerSeat))
            return true;
         //IsSeatFree returns false as soon as the partner occupies the one passegnger seat in a 2-seater.
         return _partner.isSittingInVehicle(_playerVehicle);
      }

      private void PartnerGetOnYourBike()
      {
         lock(SyncLock)
         {
            if(!CommonFunctions.PedExists(_partner))
               return;
            if(!CommonFunctions.VehicleExists(_partnerBike))
               GetPoliceBikeForPartner();
            if(!CommonFunctions.VehicleExists(_partnerBike))
               return;
            if(_partner.isSittingInVehicle(_partnerBike))
               return;
            _partner.Task.ClearAllImmediately();
            _partner.Task.AlwaysKeepTask = true;
            _partner.Task.EnterVehicle(_partnerBike, VehicleSeat.Driver);
            while(CommonFunctions.VehicleExists(_partnerBike)
                  && CommonFunctions.PedExists(_partner)
                  && !_partner.isSittingInVehicle(_partnerBike))
            {
               Wait(_actualInterval + 100); //Give the partner some time to get in.
               if(!_partner.isSittingInVehicle(_partnerBike) && !_partner.isGettingIntoAVehicle)
                  _partner.Task.EnterVehicle(_partnerBike, VehicleSeat.Driver);
            }
            _partner.Task.AlwaysKeepTask = false;
         }
      }

      private void PartnerHopOnBikeIfNotAlreadyInCaseYouAreFarAway()
      {
         if(PartnerAndPartnerVehicleExist()
            && !PartnerIsOnHisBike()
            && Player.Character.Position.DistanceTo(_partner.Position) > 10.0f
            && Player.Character.isSittingInVehicle()
            && !PartnerCanRideInPlayersVehicle())
            PartnerGetOnYourBike();
      }

      private bool PartnerIsCloseAndHeIsOnHisBikeAndPlayerIsOnFoot()
      {
         if(!PartnerAndPartnerVehicleExist())
            return false;
         if((Player.Character.Position.DistanceTo(_partner.Position) <= NEAR)
            && _partner.isSittingInVehicle(_partnerBike)
            && !Player.Character.isSittingInVehicle())
            return true;
         return false;
      }

      private bool PartnerIsInWrongVehicle()
      {
         return CommonFunctions.VehicleExists(_partnerBike)
                && _partner.CurrentVehicle != _partnerBike;
      }

      private bool PartnerIsOnHisBike()
      {
         return CommonFunctions.VehicleExists(_partnerBike) && _partner.isSittingInVehicle(_partnerBike);
      }

      private void PartnerLeaveBike()
      {
         _partner.Task.LeaveVehicle();
         _partner.Task.AlwaysKeepTask = false;
         _partner.BlockPermanentEvents = false;
      }

      private Vehicle SpawnPoliceBike()
      {
         if(!CommonFunctions.PedExists(_partner))
            return null;
         var nextPositionOnPavement = World.GetNextPositionOnPavement( _partner.Position);
         var nextPositionOnStreet = World.GetNextPositionOnStreet(_partner.Position);
         var nextPosition = nextPositionOnPavement.DistanceTo(_partner.Position) >
                            nextPositionOnStreet.DistanceTo(_partner.Position)
            ? nextPositionOnStreet
            : nextPositionOnPavement;
         if(nextPosition.DistanceTo(_partner.Position) > NEAR)
            nextPosition = _partner.Position.Around(NEAR);
         return World.CreateVehicle(new Model("POLICEB"), nextPosition);
      }

      private void SwitchTickEventToIdleState()
      {
         Interval = CommonFunctions.LONG_INTERVAL;
         _actualInterval = CommonFunctions.LONG_INTERVAL;
         Tick -= OnPartnerFollowPlayer;
         Tick += OnIdleState;
      }

      private void SwitchTickEventToPartnerFollowPlayer()
      {
         Interval = CommonFunctions.SHORT_INTERVAL;
         _actualInterval = CommonFunctions.SHORT_INTERVAL;
         Tick -= OnIdleState;
         Tick += OnPartnerFollowPlayer;
      }

      private void SynchronizePartnerSiren()
      {
         if(PartnerIsOnHisBike())
            _partnerBike.SirenActive = _playerVehicle.SirenActive;
      }

      private void TeleportIfTooFarAway()
      {
         if(!CommonFunctions.VehicleExists(_partnerBike))
            return;
         if(_partnerBike.Position.DistanceTo(Player.Character.Position) < 75)
            return;
         _partnerBike.Position = Player.Character.GetOffsetPosition(new Vector3(0, -20, 0));
         _partnerBike.PlaceOnNextStreetProperly();
         if(!CommonFunctions.VehicleExists(_playerVehicle))
            return;
         // note: I know this sucks when he is around a corner.
         _partnerBike.Heading = _playerVehicle.Heading;
         _partnerBike.Speed = _playerVehicle.Speed;
      }
   }
}