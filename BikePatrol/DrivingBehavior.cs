﻿using GTA;

namespace BikePatrol
{
   internal static class DrivingBehavior
   {
      private const float MAXIMUM_SPEED = 80.0f;

      internal static float AdaptSpeed(float playerSpeed, Vector3 playerPosition, Vector3 partnerPosition)
      {
         var distance = partnerPosition.DistanceTo(playerPosition);
         var speed = CalculateSpeed(playerSpeed, distance * 1.5f);
         return speed;
      }

      internal static Vector3 PlayerOffsetPosition(Ped player, float offsetValue)
      {
         return player.GetOffsetPosition(new Vector3(offsetValue, 0, 0));
      }

      private static float CalculateSpeed(float playerSpeed, float speedAddition)
      {
         var sum = playerSpeed + speedAddition;
         if(sum > MAXIMUM_SPEED)
            sum = MAXIMUM_SPEED;
         return sum;
      }
   }
}