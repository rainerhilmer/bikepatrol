﻿//using System;
//using System.Globalization;
//using System.Linq;
//using System.Windows.Forms;
//using GTA;
//using PolicePartnerOnBike;
//using KeyEventArgs = GTA.KeyEventArgs;

//namespace BikePatrol
//{
//   /// <summary>
//   /// This class is no vital part of the mod but just for testing and debugging purposes.
//   /// Uncomment as necessary.
//   /// </summary>
//   public class Research : Script
//   {
//      private readonly Logger _log;
//      private const float RADIUS = 5.0f;

//      public Research()
//      {
//         _log =
//            new TextLogger(
//               @"I:\wip\GTA4-Mods\PolicePartnerOnBike\PolicePartnerOnBike\PolicePartnerOnBike\bin\x86\Release\PPOB.log");
//         KeyDown += OnKeyDown;
//      }

//      private void OnKeyDown(object sender, KeyEventArgs e)
//      {
//         if(e.Key != Keys.X)
//            return;
//         if(Player.Character.CurrentVehicle != null)
//            CommonFunctions.DisplayText("Has free passenger seat? " + Player.Character.CurrentVehicle.isSeatFree(VehicleSeat.AnyPassengerSeat), 3000);
//         //CommonFunctions.DisplayText(
//         //   "Free passenger seats: "
//         //   + Player.Character.CurrentVehicle.PassengerSeats
//         //   + " || Has free passenger seat? "
//         //   + (Player.Character.CurrentVehicle.GetFreePassengerSeat() != VehicleSeat.None)
//         //   + " || Has free seat? "
//         //   + (Player.Character.CurrentVehicle.GetFreeSeat() != VehicleSeat.None), 3000);
//      }

//      private void LogCopResearch()
//      {
//         if(_log.Exists)
//            _log.Delete();
//         _log.Create("PolicePartnerOnBike log on " + DateTime.Now);
//         //LogClosestPed();
//         _log.Write("----------");
//         LogCopsFoundAround();
//         _log.Write("----------");
//         var partner = GetClosestCopWhoIsNotYou();
//         if(partner == null)
//            _log.Write("No other cop found.");
//         else
//            _log.Write("Your partner is "
//                       + partner.Position.NewDistanceTo2D(Player.Character.Position)
//                       + " units from you away.");
//         CommonFunctions.DisplayText("Log has been written.", 3000);
//      }

//      private void LogCopsFoundAround()
//      {
//         var pedsAround = CommonFunctions.GetPedsSafe(Player.Character.Position, RADIUS);
//         var copsAround = pedsAround.Where(ped => ped.RelationshipGroup == RelationshipGroup.Cop).ToList();
//         if(copsAround.Count == 0)
//         {
//            _log.Write("No cops around.");
//            return;
//         }
//         _log.Write(copsAround.Count + " cops found.");
//         _log.Write("Cop distances in relation to player position:");
//         foreach(var cop in copsAround)
//         {
//            _log.Write(cop.Position.NewDistanceTo2D(Player.Character.Position)
//               .ToString(CultureInfo.InvariantCulture));
//         }
//      }

//      /// <summary>
//      /// World.GetClosestPed returns no ped, at least not at that distance!
//      /// </summary>
//      [Obsolete("That shit ain't workin'!", error: true)]
//      private void LogClosestPed()
//      {
//         var closestPed = World.GetClosestPed(Player.Character.Position, RADIUS);
//         if(closestPed == null)
//            _log.Write("No ped around.");
//         else
//            _log.Write("Is closest ped a cop? " + (closestPed.RelationshipGroup == RelationshipGroup.Cop));
//      }

//      /// <summary>
//      /// The closest cop is always you! So get the nearest one with a distance > 0.
//      /// </summary>
//      /// <returns>That ought to be your partner.</returns>
//      private Ped GetClosestCopWhoIsNotYou()
//      {
//         var pedsAround = CommonFunctions.GetPedsSafe(Player.Character.Position, RADIUS);
//         var copsAround = pedsAround.Where(ped => ped.RelationshipGroup == RelationshipGroup.Cop).ToList();
//         Ped partner = null;
//         foreach(var cop in copsAround)
//         {
//            if(partner == null
//               && cop.Position.NewDistanceTo2D(Player.Character.Position) > 0)
//               partner = cop;
//            if(partner != null
//                && cop.Position.NewDistanceTo2D(Player.Character.Position)
//                < partner.Position.NewDistanceTo2D(Player.Character.Position))
//               partner = cop;
//         }
//         return partner;
//      }
//   }
//}

